package com.example.valentin.ppe_android;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;



/**
 * Created by pimou on 16/04/2016.
 */
public class ListClientAdapter extends ArrayAdapter<Client>{
    public ListClientAdapter(Context context, List<Client> client) {
        super(context, 0, client);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_list_client,parent, false);
        }

        ListClientViewHolder viewHolder = (ListClientViewHolder) convertView.getTag();
        if(viewHolder == null){
            viewHolder = new ListClientViewHolder();
            viewHolder.Id = (Button) convertView.findViewById(R.id.button);
            viewHolder.prenom = (TextView) convertView.findViewById(R.id.Prenom);
            viewHolder.nom = (TextView) convertView.findViewById(R.id.Nom);
            viewHolder.adresse = (TextView) convertView.findViewById(R.id.Adresse);
            viewHolder.ville = (TextView) convertView.findViewById(R.id.Ville);
            viewHolder.codePost = (TextView) convertView.findViewById(R.id.codePost);
            convertView.setTag(viewHolder);
        }

        //getItem(position) va récupérer l'item [position] de la List<Tweet> tweets
        Client client = getItem(position);
        viewHolder.Id.setTag(client.getId());
        viewHolder.prenom.setText(client.getPrenom());
        viewHolder.nom.setText(client.getnom());
        viewHolder.adresse.setText(client.getadresse());
        viewHolder.ville.setText(client.getVille());
        viewHolder.codePost.setText(client.getCodePost());

        return convertView;
    }

    private class ListClientViewHolder{
        public Button Id;
        public TextView prenom;
        public TextView nom;
        public TextView adresse;
        public TextView ville;
        public TextView codePost;

    }
}
