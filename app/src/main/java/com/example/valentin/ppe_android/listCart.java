package com.example.valentin.ppe_android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Base64;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class listCart extends Activity{


    private String Pers_Id;
    private String Client_Id;

    private ListView mListView;

    private Button backtomedic;
    private Button confirm;

    private TextView totalText;

    private float total;

    //public static final String ROOT_URL = "http://192.168.0.30/";
    public static final String ROOT_URL = "http://89.38.148.48/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_cart);

        mListView = (ListView) findViewById(R.id.listView);

        ArrayList<Medic> medicaments;
        final ArrayList<Medic> medicamentsNoQte = new ArrayList<Medic>();

        Intent intent = getIntent();
        Pers_Id = intent.getStringExtra("Pers_Id");
        Client_Id = intent.getStringExtra("Client_Id");

        medicaments = (ArrayList<Medic>)getIntent().getSerializableExtra("Medicaments");
        total = 0;

        for (int i = 0; i < medicaments.size(); i++)
        {
            Medic medic = medicaments.get(i);
            if (medic.quantite > 0) {
                medicamentsNoQte.add(medic);
                total += medic.quantite * Float.parseFloat(medic.prix);
            }
        }

        totalText = (TextView) findViewById(R.id.textView9);

        totalText.setText(Float.toString(total) + " €");



        ListCartAdapter adapter = new ListCartAdapter(listCart.this, medicamentsNoQte);
        mListView.setAdapter(adapter);

        //bouton back pour revenir à la liste des clients

        backtomedic = (Button) findViewById(R.id.backtomedics);

        backtomedic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }

        });


        confirm = (Button) findViewById(R.id.gotomail);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent vueConfirm = new Intent(listCart.this, Validmedoc.class);

                vueConfirm.putExtra("Total", Float.toString(total));
                vueConfirm.putExtra("Pers_Id", Pers_Id);
                vueConfirm.putExtra("Client_Id", Client_Id);
                vueConfirm.putExtra("Medicaments", medicamentsNoQte);
                startActivity(vueConfirm);

            }
        });


    }


}
