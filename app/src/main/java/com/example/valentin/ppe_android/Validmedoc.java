package com.example.valentin.ppe_android;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class Validmedoc extends Activity{

    private String Pers_Id;
    private String Client_Id;
    private String Total;
    private String nomClient;
    private String nomCommercial;

    private TextView total;
    private TextView client;
    private TextView commercial;
    private ArrayList<Medic> medicaments;


    private String outputJson;
    private JSONArray jsonArray;

    //public static final String ROOT_URL = "http://192.168.0.30/";
    public static final String ROOT_URL = "http://89.38.148.48/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.validationmedoc);

        Intent intent = getIntent();
        Pers_Id = (intent.getStringExtra("Pers_Id")).replaceAll("[^0-9]", "");
        Client_Id = intent.getStringExtra("Client_Id");
        Total = intent.getStringExtra("Total");
        medicaments = (ArrayList<Medic>)getIntent().getSerializableExtra("Medicaments");

        total = (TextView) findViewById(R.id.textView24);
        client = (TextView) findViewById(R.id.textView23);
        commercial = (TextView) findViewById(R.id.textView25);

        sendRequest("1", "SELECT CONCAT(`pers_Nom`,\" \", `pers_Prenom`) AS nom FROM `Personne` WHERE `pers_Id` = " + Pers_Id + " OR `pers_Id` = " + Client_Id);

        LinearLayout mContent = (LinearLayout) findViewById(R.id.signature);
        CaptureSignatureView mSig = new CaptureSignatureView(this, null);
        mContent.addView(mSig, LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        total.setText(Total + " €");


        final Button loginButton = (Button) findViewById(R.id.button2);
        loginButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final Button Confirm = (Button) findViewById(R.id.button3);
        Confirm.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                sendRequest("1", "CALL setCommande(" + Client_Id + "," + Pers_Id + ",'bla')");

                for (int i = 0; i < medicaments.size(); i++)
                {
                    Medic medic = medicaments.get(i);
                    sendRequest("1", "CALL setDetailCommande("+Client_Id+","+medic.id+","+medic.quantite+")");
                }

                AlertDialog alertDialog = new AlertDialog.Builder(Validmedoc.this).create();
                alertDialog.setTitle("Notification");
                alertDialog.setMessage("Commande terminée");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                finish();
                            }
                        });
                alertDialog.show();

            }
        });

    }

    private void sendRequest(String tag, String request){
        //Here we will handle the http request to insert user to mysql db
        //Creating a RestAdapter
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(ROOT_URL) //Setting the Root URL
                .build(); //Finally building the adapter

        //Creating object for our interface
        SqlAPi api = adapter.create(SqlAPi.class);

        request = Base64.encodeToString(request.getBytes(), Base64.DEFAULT);

        //Defining the method insertuser of our interface
        api.insertUser(

                //Passing the values by getting it from editTexts
                tag,
                request,

                //Creating an anonymous callback
                new Callback<Response>() {
                    @Override
                    public void success(Response result, Response response) {
                        //On success we will read the server's output using bufferedreader
                        //Creating a bufferedreader object
                        BufferedReader reader = null;

                        //An string to store output from the server
                        String output = "";

                        try {
                            //Initializing buffered reader
                            reader = new BufferedReader(new InputStreamReader(result.getBody().in()));

                            //Reading the output in the string
                            output = reader.readLine();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        outputJson = output;

                        try {
                            jsonArray = new JSONArray(outputJson);
                                JSONObject jsonObj = jsonArray.getJSONObject(0);
                                nomCommercial = jsonObj.getString("nom");

                                jsonObj = jsonArray.getJSONObject(1);
                                nomClient = jsonObj.getString("nom");

                            client.setText(nomClient);
                            commercial.setText(nomCommercial);

                        } catch (Throwable e) {
                            Toast.makeText(Validmedoc.this, e.toString(), Toast.LENGTH_LONG).show();
                        }

                        //Displaying the output as a toast
                        //Toast.makeText(Validmedoc.this, nomCommercial, Toast.LENGTH_LONG).show(); //popin
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        //If any error occured displaying the error as toast
                        //Toast.makeText(Android_PPE.this, error.toString(),Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

}
