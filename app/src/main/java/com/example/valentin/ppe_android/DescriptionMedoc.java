package com.example.valentin.ppe_android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class DescriptionMedoc extends Activity{

    private String outputJson;
    private JSONArray jsonArray;

    private String Medic_Id;
    //public static final String ROOT_URL = "http://192.168.0.30/";
    public static final String ROOT_URL = "http://89.38.148.48/";
    DetailMedic medicament;

    private TextView nom;
    private TextView description;
    private TextView prix;
    private Button backtomedoc;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detailmedoc);

        nom = (TextView) findViewById(R.id.textView4);
        description = (TextView) findViewById(R.id.textView7);
        prix = (TextView) findViewById(R.id.textView12);

        Intent intent = getIntent();
        Medic_Id = intent.getStringExtra("Medic_Id");
        sendRequest("1", "SELECT medicM_Nom, medicM_Description, medicM_Prix FROM Medicament WHERE MedicM_Id = " + Medic_Id );


        backtomedoc = (Button) findViewById(R.id.gobacktomedicament);

//bouton back pour revenir à la liste des clients

        backtomedoc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }

        });


    }

    private void sendRequest(String tag, String request){
        //Here we will handle the http request to insert user to mysql db
        //Creating a RestAdapter
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(ROOT_URL) //Setting the Root URL
                .build(); //Finally building the adapter

        //Creating object for our interface
        SqlAPi api = adapter.create(SqlAPi.class);

        request = Base64.encodeToString(request.getBytes(), Base64.DEFAULT);

        //Defining the method insertuser of our interface
        api.insertUser(

                //Passing the values by getting it from editTexts
                tag,
                request,

                //Creating an anonymous callback
                new Callback<Response>() {
                    @Override
                    public void success(Response result, Response response) {
                        //On success we will read the server's output using bufferedreader
                        //Creating a bufferedreader object
                        BufferedReader reader = null;

                        //An string to store output from the server
                        String output = "";

                        try {
                            //Initializing buffered reader
                            reader = new BufferedReader(new InputStreamReader(result.getBody().in()));

                            //Reading the output in the string
                            output = reader.readLine();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        outputJson = output;

                        try {
                            jsonArray = new JSONArray(outputJson);
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObj = jsonArray.getJSONObject(i);
                                medicament = new DetailMedic(jsonObj.getString("medicM_Nom"), jsonObj.getString("medicM_Description"), jsonObj.getString("medicM_Prix"));
                            }

                            nom.setText(medicament.getnom());
                            description.setText(medicament.getdescription());
                            prix.setText(medicament.getprix());



                        } catch (Throwable e) {
                            Toast.makeText(DescriptionMedoc.this, e.toString(), Toast.LENGTH_LONG).show();
                        }

                        //Displaying the output as a toast
                        //Toast.makeText(Android_PPE.this, output, Toast.LENGTH_LONG).show(); //popin
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        //If any error occured displaying the error as toast
                        //Toast.makeText(Android_PPE.this, error.toString(),Toast.LENGTH_LONG).show();
                    }
                }
        );
    }


}