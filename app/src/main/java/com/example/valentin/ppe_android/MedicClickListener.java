package com.example.valentin.ppe_android;

/**
 * Created by pimou on 02/05/2016.
 */
public interface MedicClickListener {

    void onMinusClick(Medic medic);

    void onPlusClick(Medic medic);
}
