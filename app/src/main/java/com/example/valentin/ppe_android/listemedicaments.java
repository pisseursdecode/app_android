package com.example.valentin.ppe_android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;



public class listemedicaments extends Activity{

    private String Pers_Id;
    private String Client_Id;
    private String outputJson;
    private String prenom;
    private JSONArray jsonArray;
    private Button backtoclt;

    ArrayList<Medic> medicaments = new ArrayList<Medic>();

    //public static final String ROOT_URL = "http://192.168.0.30/";
    public static final String ROOT_URL = "http://89.38.148.48/";
    ListView mListView;


    private Medics medics;
    private BaseAdapter medicsAdapter;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listemedoc);

        Intent intent = getIntent();


        Pers_Id = intent.getStringExtra("Pers_Id");
        Client_Id = intent.getStringExtra("Client_Id");

        sendRequest("1", "SELECT medicM_Id, medicM_Nom, medicM_Prix FROM Medicament");

        try {
            Thread.sleep(2000);
        }catch (Exception e){

        }
        Toast.makeText(listemedicaments.this, "Chargement terminé", Toast.LENGTH_LONG).show();

        medics = createInitialMedicList();
        medicsAdapter = new MedicsAdapter(medics, medicClickListener, getLayoutInflater());

        ListView medicsListView = (ListView) findViewById(R.id.listviewmedic);
        medicsListView.setAdapter(medicsAdapter);

        final Button tagButton=(Button) findViewById(R.id.gopanier);

        tagButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent vuePanier = new Intent(listemedicaments.this, listCart.class);


                vuePanier.putExtra("Medicaments", medicaments);

                vuePanier.putExtra("Pers_Id", Pers_Id);
                vuePanier.putExtra("Client_Id", Client_Id);
                startActivity(vuePanier);

            }
        });

        //bouton back pour revenir à la liste des clients
        backtoclt = (Button) findViewById(R.id.gobacktoclient);

        backtoclt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }

        });


}

    private Medics createInitialMedicList() {
        return new Medics(medicaments);
    }



public final MedicClickListener medicClickListener = new MedicClickListener() {
    @Override
    public void onMinusClick(Medic medic) {
        medics.removeOneFrom(medic);
        medicsAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPlusClick(Medic medic) {
        medics.addOneTo(medic);
        medicsAdapter.notifyDataSetChanged();
    }
};

    private class Medics implements MedicDataSet {

        private final List<Medic> medicList;

        Medics(List<Medic> medicList) {
            this.medicList = medicList;
        }

        @Override
        public int size() {
            return medicList.size();
        }

        @Override
        public Medic get(int position) {
            return medicList.get(position);
        }

        @Override
        public long getId(int position) {
            return position;
        }

        public void removeOneFrom(Medic medic) {
            int i = medicList.indexOf(medic);
            if (i == -1) {
                throw new IndexOutOfBoundsException();
            }
            if (medic.quantite > 0) {
                Medic updatedMedic = new Medic(medic.id, medic.nom, medic.prix, (medic.quantite - 1));
                medicList.remove(medic);
                medicList.add(i, updatedMedic);
            } else {
                AlertDialog alertDialog = new AlertDialog.Builder(listemedicaments.this).create();
                alertDialog.setTitle("Message d'erreur");
                alertDialog.setMessage("Le nombre de médicament saisis ne peut pas être inférieur à 0.");
                // Alert dialog button
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Alert dialog action goes here
                                // onClick button code here
                                dialog.dismiss();// use dismiss to cancel alert dialog
                            }
                        });
                alertDialog.show();
            }
        }

        public void addOneTo(Medic medic) {
            int i = medicList.indexOf(medic);
            if (i == -1) {
                throw new IndexOutOfBoundsException();
            }
            Medic updatedMedic = new Medic(medic.id ,medic.nom,medic.prix, (medic.quantite + 1));
            medicList.remove(medic);
            medicList.add(i, updatedMedic);
        }
    }












    public void goPanier(View v) {
        Intent vueDetailMedic = new Intent(listemedicaments.this, listCart.class);
        vueDetailMedic.putExtra("Pers_Id", Pers_Id);
        vueDetailMedic.putExtra("Client_Id", Client_Id);
        startActivity(vueDetailMedic);
    }

    public void infoMedic(View v)
    {
        Button tagButton=(Button)v.findViewById(R.id.button);
        Intent vueDetailMedic = new Intent(listemedicaments.this, DescriptionMedoc.class);
        vueDetailMedic.putExtra("Medic_Id", tagButton.getTag().toString());
        startActivity(vueDetailMedic);
    }


    private void sendRequest(String tag, String request){
        //Here we will handle the http request to insert user to mysql db
        //Creating a RestAdapter
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(ROOT_URL) //Setting the Root URL
                .build(); //Finally building the adapter

        //Creating object for our interface
        SqlAPi api = adapter.create(SqlAPi.class);

        request = Base64.encodeToString(request.getBytes(), Base64.DEFAULT);

        //Defining the method insertuser of our interface
        api.insertUser(

                //Passing the values by getting it from editTexts
                tag,
                request,

                //Creating an anonymous callback
                new Callback<Response>() {
                    @Override
                    public void success(Response result, Response response) {
                        //On success we will read the server's output using bufferedreader
                        //Creating a bufferedreader object
                        BufferedReader reader = null;

                        //An string to store output from the server
                        String output = "";

                        try {
                            //Initializing buffered reader
                            reader = new BufferedReader(new InputStreamReader(result.getBody().in()));

                            //Reading the output in the string
                            output = reader.readLine();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        outputJson = output;

                        try {
                            jsonArray = new JSONArray(outputJson);
                            for(int i = 0; i < jsonArray.length(); i++)
                            {
                                JSONObject jsonObj = jsonArray.getJSONObject(i);
                                Medic medicament = new Medic(jsonObj.getString("medicM_Id"),jsonObj.getString("medicM_Nom"),jsonObj.getString("medicM_Prix"),0);
                                medicaments.add(medicament);

                            }
                        }catch (Throwable e) {
                            Toast.makeText(listemedicaments.this, e.toString(), Toast.LENGTH_LONG).show();
                        }
                        //Displaying the output as a toast
                        //Toast.makeText(Android_PPE.this, output, Toast.LENGTH_LONG).show(); //popin
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        //If any error occured displaying the error as toast
                        //Toast.makeText(Android_PPE.this, error.toString(),Toast.LENGTH_LONG).show();
                    }
                }
        );
    }


}
