package com.example.valentin.ppe_android;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by pimou on 17/04/2016.
 */
public class Cart {

    private List<Medic> listMedic = new ArrayList<Medic>();

    public void addMedic(Medic medic) {
        listMedic.add(medic);
    }

    public List<Medic> getListMedic() {
        return listMedic;
    }

}
