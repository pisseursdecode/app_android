package com.example.valentin.ppe_android;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;


/**
 * Created by pimou on 16/04/2016.
 */
public class ListCartAdapter extends ArrayAdapter<Medic>{
    public ListCartAdapter(Context context, List<Medic> medics) {
        super(context, 0, medics);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.row_list_cart,parent, false);
        }

        ListCartViewHolder viewHolder = (ListCartViewHolder) convertView.getTag();
        if(viewHolder == null){
            viewHolder = new ListCartViewHolder();
            viewHolder.Quantite = (TextView) convertView.findViewById(R.id.textView18);
            viewHolder.Nom = (TextView) convertView.findViewById(R.id.textView15);
            viewHolder.Prix = (TextView) convertView.findViewById(R.id.textView17);
            viewHolder.Total = (TextView) convertView.findViewById(R.id.product_name);
            convertView.setTag(viewHolder);
        }

        //getItem(position) va récupérer l'item [position] de la List<Tweet> tweets
        Medic medic = getItem(position);
        viewHolder.Nom.setText(medic.nom);
        viewHolder.Prix.setText(medic.prix + " €");
        viewHolder.Quantite.setText(Integer.toString(medic.quantite));
        viewHolder.Total.setText(Float.toString(medic.quantite * Float.parseFloat(medic.prix)) + " €");

        return convertView;
    }

    private class ListCartViewHolder{
        public TextView Quantite;
        public TextView Nom;
        public TextView Prix;
        public TextView Total;
    }
}
