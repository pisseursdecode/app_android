package com.example.valentin.ppe_android;



import java.io.Serializable;

/**
 * Created by pimou on 17/04/2016.
 */
public class Medic implements Serializable {

    final String id;
    final String nom;
    final String prix;
    final int quantite;

    public Medic(String id, String nom, String prix, int quantite) {
        this.id  = id;
        this.nom = nom;
        this.prix = prix;
        this.quantite = quantite;
    }



}
