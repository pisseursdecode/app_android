package com.example.valentin.ppe_android;

        import android.app.Activity;
        import android.content.Intent;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.util.Base64;
        import android.view.Menu;
        import android.view.MenuItem;
        import android.view.View;
        import android.widget.AdapterView;
        import android.widget.ArrayAdapter;
        import android.widget.Button;
        import android.widget.ListView;
        import android.widget.Toast;

        import org.json.JSONArray;
        import org.json.JSONException;
        import org.json.JSONObject;

        import java.io.BufferedReader;
        import java.io.IOException;
        import java.io.InputStreamReader;
        import java.util.ArrayList;
        import java.util.List;

        import retrofit.Callback;
        import retrofit.RestAdapter;
        import retrofit.RetrofitError;
        import retrofit.client.Response;

public class listeclts extends Activity{

    private String Pers_Id;
    private String outputJson;
    private String prenom;
    private JSONArray jsonArray;

    ListView mListView;
    List<Client> clients = new ArrayList<Client>();;

   // public static final String ROOT_URL = "http://192.168.0.30/";
   public static final String ROOT_URL = "http://89.38.148.48/";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_client);

        mListView = (ListView) findViewById(R.id.listView);

        Intent intent = getIntent();
        Pers_Id = intent.getStringExtra("Pers_Id");
        sendRequest("1", "CALL getClients()");

    }

    public void myClickHandler(View v)
    {
        Button tagButton=(Button)v.findViewById(R.id.button);
        Intent vueListMedic = new Intent(listeclts.this, listemedicaments.class);
        vueListMedic.putExtra("Pers_Id", Pers_Id);
        vueListMedic.putExtra("Client_Id", tagButton.getTag().toString());
        startActivity(vueListMedic);
    }

    private void afficherListeClients() {

        ListClientAdapter adapter = new ListClientAdapter(listeclts.this, clients);
        mListView.setAdapter(adapter);
    }


    private void sendRequest(String tag, String request){
        //Here we will handle the http request to insert user to mysql db
        //Creating a RestAdapter
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(ROOT_URL) //Setting the Root URL
                .build(); //Finally building the adapter

        //Creating object for our interface
        SqlAPi api = adapter.create(SqlAPi.class);

        request = Base64.encodeToString(request.getBytes(), Base64.DEFAULT);

        //Defining the method insertuser of our interface
        api.insertUser(

                //Passing the values by getting it from editTexts
                tag,
                request,

                //Creating an anonymous callback
                new Callback<Response>() {
                    @Override
                    public void success(Response result, Response response) {
                        //On success we will read the server's output using bufferedreader
                        //Creating a bufferedreader object
                        BufferedReader reader = null;

                        //An string to store output from the server
                        String output = "";

                        try {
                            //Initializing buffered reader
                            reader = new BufferedReader(new InputStreamReader(result.getBody().in()));

                            //Reading the output in the string
                            output = reader.readLine();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        outputJson = output;

                        try {
                            jsonArray = new JSONArray(outputJson);
                            for(int i = 0; i < jsonArray.length(); i++)
                            {
                                JSONObject jsonObj = jsonArray.getJSONObject(i);
                                Client client = new Client(jsonObj.getString("pers_Id"),jsonObj.getString("pers_Prenom"),jsonObj.getString("pers_Nom"),jsonObj.getString("pers_Adresse"),jsonObj.getString("pers_Ville"),jsonObj.getString("pers_Cp"));
                                clients.add(client);
                            }

                            afficherListeClients();
                        }catch (Throwable e) {
                            Toast.makeText(listeclts.this, e.toString(), Toast.LENGTH_LONG).show();
                        }

                        //Displaying the output as a toast
                        //Toast.makeText(Android_PPE.this, output, Toast.LENGTH_LONG).show(); //popin
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        //If any error occured displaying the error as toast
                        //Toast.makeText(Android_PPE.this, error.toString(),Toast.LENGTH_LONG).show();
                    }
                }
        );
    }


}
