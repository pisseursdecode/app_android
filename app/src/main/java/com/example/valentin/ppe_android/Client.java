package com.example.valentin.ppe_android;

/**
 * Created by pimou on 16/04/2016.
 */
public class Client {
    private String Id;
    private String prenom;
    private String nom;
    private String adresse;
    private String ville;
    private String codePost;

    public Client(String Id, String prenom, String nom, String adresse, String ville, String codePost) {
        this.Id = Id;
        this.prenom = prenom;
        this.nom = nom;
        this.adresse = adresse;
        this.ville = ville;
        this.codePost = codePost;
    }

    public String getId() {
        return Id;
    }

    public void setId(String Id) {
        this.Id = Id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getnom() {
        return nom;
    }

    public void setnom(String nom) {
        this.nom = nom;
    }

    public String getadresse() {
        return adresse;
    }

    public void setadresse(String adresse) {
        this.adresse = adresse;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCodePost() {
        return codePost;
    }

    public void setCodePost(String codePost) {
        this.codePost = codePost;
    }
}
