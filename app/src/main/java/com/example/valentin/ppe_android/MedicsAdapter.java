package com.example.valentin.ppe_android;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

class MedicsAdapter extends BaseAdapter {

    private final MedicDataSet medicDataSet;
    private final MedicClickListener medicClickListener;
    private final LayoutInflater layoutInflater;

    MedicsAdapter(MedicDataSet medicDataSet, MedicClickListener medicClickListener, LayoutInflater layoutInflater) {
        this.medicDataSet = medicDataSet;
        this.medicClickListener = medicClickListener;
        this.layoutInflater = layoutInflater;
    }

    @Override
    public int getCount() {
        return medicDataSet.size();
    }

    @Override
    public Medic getItem(int position) {
        return medicDataSet.get(position);
    }

    @Override
    public long getItemId(int position) {
        return medicDataSet.getId(position);
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = createView(parent);
            view.setTag(ViewHolder.from(view));
        }
        Medic medic = medicDataSet.get(position);
        ViewHolder viewHolder = (ViewHolder) view.getTag();
        update(viewHolder, medic);
        return view;
    }

    private View createView(ViewGroup parent) {
        return layoutInflater.inflate(R.layout.row_list_medicament, parent, false);
    }

    private void update(ViewHolder viewHolder, final Medic medic) {
        viewHolder.name.setText(medic.nom);
        viewHolder.quantity.setText(String.valueOf(medic.quantite));
        viewHolder.price.setText(medic.prix);
        viewHolder.info.setTag(medic.id);
        viewHolder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                medicClickListener.onMinusClick(medic);
            }
        });
        viewHolder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                medicClickListener.onPlusClick(medic);
            }
        });
    }

    private static final class ViewHolder {
        final TextView name;
        final TextView quantity;
        final TextView price;
        final View info;
        final View minus;
        final View plus;

        static ViewHolder from(View view) {
            return new ViewHolder(
                    ((TextView) view.findViewById(R.id.product_name)),
                    ((TextView) view.findViewById(R.id.product_quantity)),
                    ((TextView) view.findViewById(R.id.product_price)),
                    view.findViewById(R.id.button),
                    view.findViewById(R.id.product_minus),
                    view.findViewById(R.id.product_plus)

            );
        }

        private ViewHolder(TextView name, TextView quantity, TextView price, View info, View minus, View plus) {
            this.name = name;
            this.quantity = quantity;
            this.price = price;
            this.info = info;
            this.minus = minus;
            this.plus = plus;
        }
    }

}