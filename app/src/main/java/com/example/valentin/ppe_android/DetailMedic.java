package com.example.valentin.ppe_android;

/**
 * Created by pimou on 16/04/2016.
 */
public class DetailMedic {
    private String nom;
    private String description;
    private String prix;

    public DetailMedic(String nom, String description, String prix) {
        this.nom = nom;
        this.description = description;
        this.prix = prix;
    }


    public String getnom() {
        return nom;
    }

    public void setnom(String nom) {
        this.nom = nom;
    }

    public String getdescription() {
        return description;
    }

    public void setdescription(String description) {
        this.description = description;
    }

    public String getprix() {
        return prix;
    }

    public void setprix(String prix) {
        this.prix = prix;
    }

}
