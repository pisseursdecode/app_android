package com.example.valentin.ppe_android;

/**
 * Created by pimou on 02/05/2016.
 */
interface MedicDataSet {

    int size();

    Medic get(int position);

    long getId(int position);

}