package com.example.valentin.ppe_android;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class Android_PPE extends AppCompatActivity {

    //public static final String ROOT_URL = "http://192.168.0.30/";
    public static final String ROOT_URL = "http://89.38.148.48/";

    private Button loginButton;
    private EditText login;
    private EditText mdp;

    private String Id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_android__ppe);

        loginButton = (Button) findViewById(R.id.gotoclients);
        login = (EditText) findViewById(R.id.editText);
        mdp = (EditText) findViewById(R.id.editText2);


        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendRequest("1", "SELECT pers_Id FROM collaborateurs WHERE col_Login = '" + login.getText().toString() + "' AND col_Mdp = '" + mdp.getText().toString() + "'");

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_android__ppe, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void sendRequest(String tag, String request){
        //Here we will handle the http request to insert user to mysql db
        //Creating a RestAdapter
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(ROOT_URL) //Setting the Root URL
                .build(); //Finally building the adapter

        //Creating object for our interface
        SqlAPi api = adapter.create(SqlAPi.class);

        request = Base64.encodeToString(request.getBytes(), Base64.DEFAULT);

        //Defining the method insertuser of our interface
        api.insertUser(

                //Passing the values by getting it from editTexts
                tag,
                request,

                //Creating an anonymous callback
                new Callback<Response>() {
                    @Override
                    public void success(Response result, Response response) {
                        //On success we will read the server's output using bufferedreader
                        //Creating a bufferedreader object
                        BufferedReader reader = null;

                        //An string to store output from the server
                        String output = "";

                        try {
                            //Initializing buffered reader
                            reader = new BufferedReader(new InputStreamReader(result.getBody().in()));

                            //Reading the output in the string
                            output = reader.readLine();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        Id = output;

                        if (output.length() > 2) {

                            Toast.makeText(Android_PPE.this, "Vous êtes connectés", Toast.LENGTH_LONG).show(); //popin

                            Intent intent = new Intent(Android_PPE.this, listeclts.class);
                            intent.putExtra("Pers_Id", Id);
                            startActivity(intent);
                        }else {
                            Toast.makeText(Android_PPE.this, "Mauvais identifiants", Toast.LENGTH_LONG).show(); //popin
                        }

                        //Displaying the output as a toast
                        //Toast.makeText(Android_PPE.this, output, Toast.LENGTH_LONG).show(); //popin
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        //If any error occured displaying the error as toast
                        //Toast.makeText(Android_PPE.this, error.toString(),Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

}
