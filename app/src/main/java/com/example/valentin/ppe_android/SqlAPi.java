package com.example.valentin.ppe_android;

import retrofit.Callback;
import retrofit.client.Response;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by pimou on 13/04/2016.
 */
public interface SqlAPi {

    @FormUrlEncoded
    @POST("/ppe/phpMobileAndroid.php")
    public void insertUser(
            @Field("tag") String tag, //1 = Select
            @Field("request") String request,
            Callback<Response> callback);

}
